<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;//ResourceCollection;

class ProductCollection extends JsonResource //ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        
        return [
            'title'         =>  $this->name,
            'rating'        =>  $this->reviews->count() > 0 ? round($this->reviews->sum('star')/ $this->reviews->count()) : 'No Review yet',
            'discount'      =>  $this->discount,
            'Price'    =>  (1- ($this->discount/100)) * $this->price,
            'href'          => [
                'review'    =>  route('reviews.index', $this->id),
            ]
        ];
    }
}
