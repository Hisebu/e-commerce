<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ecommerce\Repository\Contracts\ProductRepositoryInterface;
// use App\Http\Requests\ProductRequest;
// use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Product\ProductCollection;
use Symfony\Component\HttpFoundation\Response;
// use App\Exceptions\CustomException;
use Auth;

class ProductController extends Controller
{
    
private $productRepository;

public function __construct(ProductRepositoryInterface $productRepository)
{
    $this->middleware('auth:api')->except('index', 'show');
    $this->productRepository = $productRepository;
}


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = $this->productRepository->getAll();
        return ProductCollection::Collection($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        //
        $product = Product::create([
            'name' => $request->get('name'),
            'detail' => $request->get('description'),
            'price' => $request->get('price'),
            'stock' => $request->get('stock'),
            'discount' => $request->get('discount')
        ]);

        return response()->json(['$data' => new ProductResource($product)], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {
        $product = $this->productRepository->findById($product);
        // dd($product);
        // return $product;
        return response()->json(['$data' => new ProductResource($product)], Response::HTTP_OK);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {

        if (Auth::id() !== $product->user_id){

            throw new CustomException; //exception not made yet
        }else {

            $request['detail'] = $request->description;
        unset($request['description']);

        $product->update($request->all());
       
        return response()->json(['$data' => new ProductResource($product)], Response::HTTP_CREATED);

        }
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        if (Auth::id() !== $product->user_id){

            throw new CustomException; //exception not made yet
        }else {

            $product->delete();
            return response()->json([null, Response::HTTP_NO_CONTENT]);

        }
    }
}





// na the formal code be this


// <?php

// namespace App\Http\Controllers\Api;

// use App\Http\Controllers\Controller;
// use App\ecommerce\Models\Product;
// use App\ecommerce\Repository\ProductRepository;
// use App\Http\Requests\ProductRequest;
// use App\Http\Requests\UpdateProductRequest;
// use App\Http\Resources\Product\ProductResource;
// use App\Http\Resources\Product\ProductCollection;
// use Symfony\Component\HttpFoundation\Response;
// use App\Exceptions\CustomException;
// use Auth;

// class ProductController extends Controller
// {
//     private $productRepository;

//     public function __construct(ProductRepository $productRepository)
//     {
//         $this->middleware('auth:api')->except('index', 'show');
//         $this->productRepository = $productRepository;
//     }
//     /**
//      * Display a listing of the resource.
//      *
//      * @return \Illuminate\Http\Response
//      */
//     public function index()
//     {
//         //
//         return ProductCollection::Collection(Product::paginate(20));
//     }

//     /**
//      * Show the form for creating a new resource.
//      *
//      * @return \Illuminate\Http\Response
//      */
//     public function create()
//     {
//         //
//     }

//     /**
//      * Store a newly created resource in storage.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @return \Illuminate\Http\Response
//      */
//     public function store(ProductRequest $request)
//     {
//         //
//         $product = Product::create([
//             'name' => $request->get('name'),
//             'detail' => $request->get('description'),
//             'price' => $request->get('price'),
//             'stock' => $request->get('stock'),
//             'discount' => $request->get('discount')
//         ]);

//         return response()->json(['$data' => new ProductResource($product)], Response::HTTP_CREATED);
//     }

//     /**
//      * Display the specified resource.
//      *
//      * @param  \App\Models\Product  $product
//      * @return \Illuminate\Http\Response
//      */
//     public function show(Product $product)
//     {
//         //
//         //return $product;
//         //return new ProductResource($product);
//         return response()->json(['$data' => new ProductResource($product)], Response::HTTP_OK);
        
//     }

//     /**
//      * Show the form for editing the specified resource.
//      *
//      * @param  \App\Models\Product  $product
//      * @return \Illuminate\Http\Response
//      */
//     public function edit(Product $product)
//     {
//         //
//     }

//     /**
//      * Update the specified resource in storage.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @param  \App\Models\Product  $product
//      * @return \Illuminate\Http\Response
//      */
//     public function update(UpdateProductRequest $request, Product $product)
//     {

//         if (Auth::id() !== $product->user_id){

//             throw new CustomException; //exception not made yet
//         }else {

//             $request['detail'] = $request->description;
//         unset($request['description']);

//         $product->update($request->all());
       
//         return response()->json(['$data' => new ProductResource($product)], Response::HTTP_CREATED);

//         }
        

//     }

//     /**
//      * Remove the specified resource from storage.
//      *
//      * @param  \App\Models\Product  $product
//      * @return \Illuminate\Http\Response
//      */
//     public function destroy(Product $product)
//     {
//         //
//         if (Auth::id() !== $product->user_id){

//             throw new CustomException; //exception not made yet
//         }else {

//             $product->delete();
//             return response()->json([null, Response::HTTP_NO_CONTENT]);

//         }
//     }
// }
