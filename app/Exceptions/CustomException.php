<?php

namespace App\Exceptions;

use Exception;

class CustomException extends Exception
{

    public function render()
    {
        return ['error' => 'Product Don\'t Belong To User'];
    }

}
