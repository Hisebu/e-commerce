<?php

namespace App\ecommerce\Repository;

use App\ecommerce\Models\Product;
use App\ecommerce\Repository\Contracts\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    public function getAll()
    {
        return Product::paginate(20);
    }

    public function findById($product)
    {
        // return response()->json(['$data' => new ProductResource($product)], Response::HTTP_OK);
        return Product::where('id', $product)->firstOrFail();

    }

    public function requestEnity($request)
    {

    }

}