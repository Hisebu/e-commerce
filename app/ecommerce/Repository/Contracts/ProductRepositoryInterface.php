<?php

// namespace App\ecommerce\Repository\Interfaces;

// // use App\Http\Requests\UserRequest;

// interface UserInterface
// {
//     /**
//      * Get all 
//      * 
//      * @method  GET api/model
//      * @access  public
//      */
//     public function getAll();

//     /**
//      * Get  By ID
//      * 
//      * @param   integer     $id
//      * 
//      * @method  GET api/users/{id}
//      * @access  public
//      */
//     public function getById($id);

//     /**
//      * Create | Update 
//      * 
//      * @param   \App\Http\Requests    $request
//      * @param   integer                           $id
//      * 
//      * @method  POST    api/users       For Create
//      * @method  PUT     api/users/{id}  For Update     
//      * @access  public
//      */
//     public function requestUser($request, $id = null);

//     /**
//      * Delete user
//      * 
//      * @param   integer     $id
//      * 
//      * @method  DELETE  api/users/{id}
//      * @access  public
//      */
//     public function delete($id);
// }





namespace App\ecommerce\Repository\Contracts;

interface ProductRepositoryInterface
{
    public function getAll();

    public function findById($product);

    public function requestEnity($request);
}