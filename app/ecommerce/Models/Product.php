<?php

namespace App\ecommerce\Models;

use App\ecommerce\Models\Review;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'detail', 'price', 'stock', 'discount'
    ];

    public function reviews() {

        return $this->hasMany(Review::class);
    
    }
}
