<?php

namespace App\ecommerce\Models;

use App\ecomerce\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer', 'reviews', 'star',
    ];

    public function products(){

        return $this->belongTo(Product::class);
    
    }

}
