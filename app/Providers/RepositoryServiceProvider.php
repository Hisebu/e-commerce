<?php

namespace App\Providers;

use App\ecommerce\Repository\ProductRepository;
use App\ecommerce\Repository\Contracts\ProductRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
    }
}
